function BattleScene() {
    this.scene = null;
    this.camera = null;
    this.renderer = null;
    
    var fog = new THREE.Fog(0x000000, 4, 25);
    
    this.init = function() {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.renderer = new THREE.WebGLRenderer( { antialias: true } );
        //this.renderer = new THREE.WebGLRenderer();
        this.renderer.shadowMapEnabled = true;
        this.renderer.shadowMapType = THREE.PCFShadowMap;
        this.renderer.shadowMapSoft = true;        
        this.renderer.setClearColor( fog.color, 1 );
		this.renderer.autoClear = false;
        
        this.scene.add(this.camera);
        this.camera.position.set(0, 0, -5);
        this.camera.lookAt(new THREE.Vector3(0, 0, 0));
    };

    this.prepareHtml = function() {
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(this.renderer.domElement);
    };

    this.addLight = function() {
        var ligth = new THREE.AmbientLight(0x223322);
        this.scene.add(ligth);

        var direct = new THREE.SpotLight(0xffffff, 0.5);
        direct.position.set(0, 7, 4);
        direct.target.position.set(0, 0, 2);
        direct.castShadow = true;
        direct.shadowDarkness = 0.5;
        direct.shadowCameraVisible = true;
        direct.shadowBias = 0.001;
        
        // these six values define the boundaries of the yellow box seen above
        direct.shadowCameraNear = 2;
        direct.shadowCameraFar = 10;
        direct.shadowCameraLeft = -1.5;
        direct.shadowCameraRight = 1.5;
        direct.shadowCameraTop = 1.5;
        direct.shadowCameraBottom = -1.5;
        
        this.scene.add(direct);
        this.scene.add(direct.target);
        
        var d = new THREE.SpotLight(0xffffff, 0.5, 40, 0.1);
        d.position.set(0, 0, -20);
        this.scene.add(d);
    };
    
    this.addFog = function() {
        this.scene.fog = fog;//new THREE.FogExp2(0xefd1b5, 0.05);
    }
    
    this.addPlane = function() {
        var geometry = new THREE.PlaneBufferGeometry( 5, 20, 32 );
        var material = new THREE.MeshPhongMaterial( {color: 0x0000ff, side: THREE.DoubleSide} );
        material.ambient = material.color;
        var plane = new THREE.Mesh(geometry, material);
        this.scene.add(plane);
        plane.rotation.x = -90 * (Math.PI/180);
        plane.position.z = 10;
        plane.position.y = -1;
        
        plane.castShadow = false;
        plane.receiveShadow = true;
    }
    
    this.render = function() {
        this.renderer.render(this.scene, this.camera);
    }

    this.init();
    this.prepareHtml();
    this.addLight();
    this.addFog();
    //this.addPlane();
}
