function MazeBuilder(scene) {
    this.mazePlan = [
        '***********', //1
        '*1        *',
        '*         *',
        '*         *',
        '*         *',
        '*         *', // 5
        '*         *',
        '*         *',
        '*         *',
        '*        2*',
        '***********'
    ];
    
    var addBack = function(x, y, w, h) {
        var geometry = new THREE.PlaneBufferGeometry(w, h);
        var material = new THREE.MeshPhongMaterial( {color: 0x0000ff, side: THREE.DoubleSide} );
        material.ambient = material.color;
        var plane = new THREE.Mesh(geometry, material);
        
        scene.scene.add(plane);

        //plane.rotation.x = -90 * (Math.PI/180);
        plane.position.x = x;
        plane.position.y = y;
        
        plane.castShadow = false;
        plane.receiveShadow = true;
    }
    
    var init = function(mazePlan, adder) {
        var startUp = ~~(mazePlan.length / 2);
        $.each(mazePlan, function(y, line) {
            var x = ~~(line.length / 2);
            adder(-x, y - startUp, line.length, 1);
        });
    }
    
    init(this.mazePlan, addBack);
           
}