function KeyEventHander(scene, animation) {
    $(document).keydown(function(event) {
        selectMove(event.which, true);
    });
    $(document).keyup(function(event) {
        selectMove(event.which, false);
    })
    
    function selectMove(key, down) {
        switch (key) {
            case 37:
                onLeft(down);
                break;
            case 38:
                onUp(down);
                break;
            case 39:
                onRight(down);
                break;
            case 40:
                onDown(down);
                break;
                
            case 85:
                cameraForward(down);
                break;
            case 77:
                cameraBackward(down);
                break;
                
            default:
                console.log('unhandled key: ' + key);
                
        }
    }
    function onLeft(speedUp) {
        //animation.startMove(animation.cameraTarget, 'yaw', speedUp ? 1 : 0);
    }
    
    function onUp(speedUp) {
        //animation.startMove(animation.cameraTarget, 'pitch', speedUp ? 1 : 0);
    }
    
    function onRight(speedUp) {
        //animation.startMove(animation.cameraTarget, 'yaw', speedUp ? -1 : 0);
    }
    
    function onDown(speedUp) {
        //animation.startMove(animation.cameraTarget, 'pitch', speedUp ? -1 : 0);
    }
    
    function cameraForward(isSpeeding) {
        animation.startMove(scene.camera, 'z', isSpeeding ? 1 : 0);
    }
    function cameraBackward(isSpeeding) {
        animation.startMove(scene.camera, 'z', isSpeeding ? -1 : 0);
    }
}

/*    
    function onLeft(speedUp) {
        animation.startMove(animation.cube, 'x', speedUp ? 1 : 0);
    }
    
    function onUp(speedUp) {
        animation.startMove(animation.cube, 'z', speedUp ? 1 : 0);
    }
    
    function onRight(speedUp) {
        animation.startMove(animation.cube, 'x', speedUp ? -1 : 0);
    }
    
    function onDown(speedUp) {
        animation.startMove(animation.cube, 'z', speedUp ? -1 : 0);
    }
}
*/
