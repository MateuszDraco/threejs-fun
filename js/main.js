function loadUp() {
    $.when(
        $.getScript("js/BattleScene.js", function() { console.log("scene builder loaded")}),
        $.getScript("js/SimpleCube.js", function() { console.log("cube loaded") }),
        $.getScript("js/SimpleAnimation.js", function() { console.log("animation loaded") }),
        $.getScript("js/KeyEventHandler.js", function() { console.log("key handlers loaded") }),
        $.getScript("js/MazeBuilder.js", function() { console.log("maze builder loaded") })
    ).then(function() {
        console.log("all loaded up")
        main()
    });
}

var animation;

function main() {
    var scene = new BattleScene();
    animation = new SimpleAnimation(scene);
    new KeyEventHander(scene, animation);
    new MazeBuilder(scene);
    render();
}

var render = function() {
    requestAnimationFrame(render);
    animation.animate();
}


