function SimpleAnimation(scene) {
    this.cube = new SimpleCube(scene.scene).cube

    this.cube.position.z = 2;
    this.cube.position.x = 0;
    this.cube.position.y = 0;

    this.animate = function() {
        this.cube.rotation.x += 0.01
        this.cube.rotation.y += 0.015
        moveObjects();

        scene.render();
    }

    var moves = new Array();
    var rotations = {};
    
    this.move = function(model, direction, value) {
        var found = false;
        
        $.each(moves, function(i, v) {
            if (v.model == model && v.direction == direction) {
                found = true;
                v.speed = value;
                v.desired = 0;
                return false;
            }
        });
        if (!found)
            moves.push({ obj: model, direction: direction, speed: value, desired: 0});
    }
    
    this.startMove = function(model, direction, desiredSpeed) {
        var found = false;
        
        $.each(moves, function(i, v) {
            if (v.obj == model && v.direction == direction) {
                found = true;
                v.desired = desiredSpeed;
                return false;
            }
        });
        if (!found)
            moves.push({ obj: model, direction: direction, speed: 0, desired: desiredSpeed});
    }
    
    var step = 0.1;
    
    function moveObjects() {
        var toRemove = new Array();
        $.each(moves, function(num, val) {
            if (val.desired != val.speed) {
                var xx = val.speed / 10.0;

                switch (val.direction) {
                    case 'x':
                        val.obj.position.x += xx;
                        break;
                    case 'y':
                        val.obj.position.y += xx;
                        break;
                    case 'z':
                        val.obj.position.z += xx;
                        break;
                    case 'ry':
                        val.obj.rotation.y += xx;
                        break;
                    case 'rx':
                        val.obj.rotation.x += xx;
                        break;
                    case 'rz':
                        val.obj.rotation.z += xx;
                        break;
                        
                    case 'yaw':
                        val.obj.alterYaw(xx);
                        break;
                    case 'pitch':
                        val.obj.alterPitch(xx);
                        break;
                }
                val.speed += (val.desired - val.speed) / 10.0;

                if ((val.desired > 0 && val.speed > val.desired)
                   || (val.desired < 0 && val.speed < val.desired))
                    val.speed = val.desired;
            }
            
            if (val.desired == 0 && Math.abs(val.speed) < 0.01)
                toRemove.push(val);
        });
        
        $.each(toRemove, function(i, rem){
            var index = moves.indexOf(rem);
            if (index >= 0)
                moves.splice(index, 1);
        });
        
    }
    
}