function SimpleCube(scene) {
    this.geometry = new THREE.BoxGeometry(1, 1, 1);
    this.material = new THREE.MeshPhongMaterial( { color: 0xffff00 } )
    this.cube = new THREE.Mesh(this.geometry, this.material);
    this.cube.castShadow = true;

    scene.add(this.cube);
}